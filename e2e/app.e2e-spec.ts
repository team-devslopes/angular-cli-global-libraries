import { GlobalLibrariesPage } from './app.po';

describe('global-libraries App', function() {
  let page: GlobalLibrariesPage;

  beforeEach(() => {
    page = new GlobalLibrariesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
