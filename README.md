# Slope Session - Angular Cli Webpack: Adding Global Libraries
This is the source code for an Angular 2 tutorial that shows how to add global libraries (Twitter's Bootstrap v4 in this example) to a project that is created using the Angular Webpack Cli.

>**Note:** It is essential that you use version 8+ of the **Webpack** version of the Angular Cli for this tutorial (earlier versions **won't** work).

Thanks

Jess Rascal
